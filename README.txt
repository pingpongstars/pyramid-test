pyramid_test
============

Getting Started
---------------

- Change directory into your newly created project.

    cd pyramid_test

- Create a Python virtual environment.

    python3 -m venv env

- Upgrade packaging tools.

    env/bin/pip install --upgrade pip setuptools

- Install the project in editable mode with its testing requirements.

    env/bin/pip install -e ".[testing]"

- Initialize and upgrade the database using Alembic.

    - Generate your first revision.

        env/bin/alembic -c development.ini revision --autogenerate -m "init"

    - Upgrade to that revision.

        env/bin/alembic -c development.ini upgrade head

- Load default data into the database using a script.

    env/bin/initialize_pyramid_test_db development.ini

- Run your project's tests.

    env/bin/pytest

- Run your project.

    env/bin/pserve development.ini







I18n:

requires
apt install gettext
pip install babel

#extract strings from py, jinja2, mako
pybabel extract -F babel.ini -o ${project}/locale/${project}.pot ${project} 
pybabel extract -F babel.ini -k _t:2 -o ${project}/locale/${project}.pot ${project} 

#first time creation
mkdir -p ${project}/${lang}/LC_MESSAGES
msginit -l ${lang} -o ${project}/locale/${lang}/LC_MESSAGES/${project}.po --input ${project}/locale/${project}.pot

#update
msgmerge --update ${project}/locale/${lang}/LC_MESSAGES/${project}.po ${project}/locale/${project}.pot

#compile .po into .mo
msgfmt -o ${project}/locale/${lang}/LC_MESSAGES/${project}.mo ${project}/locale/${lang}/LC_MESSAGES/${project}.po