from pyramid.view import view_config
from pyramid.response import Response

from sqlalchemy.exc import DBAPIError

from .. import models
from ..utils import (_, __)

import os,datetime,logging
l = logging.getLogger(__name__)

def my_view_jinja(request):
    try:
        query = request.dbsession.query(models.MyModel)
        one = query.filter(models.MyModel.name == 'one').first()
    except DBAPIError:
        return Response(db_err_msg, content_type='text/plain', status=500)
    return {'one': one, 'project': 'pyramid_test'}

class Controller():
    def __init__(self,request):
        self.request = request
        self.localizer = request.localizer


    @view_config(route_name='home', renderer='../templates/mytemplate.jinja2')
    @view_config(route_name='home_mako', renderer='../templates/mytemplate.mako')
    def my_view_mako(self):
        request = self.request
        try:
            #request._LOCALE_ = 'it'
            l.info("locale_name = {}".format( request.locale_name ) )
            l.info("localizer locale_name = {}".format( request.localizer.locale_name ) )
            #_ = request.localizer
            query = request.dbsession.query(models.MyModel)
            one = query.filter(models.MyModel.name == 'one').first()
            ts = _('stringa traducibile di test')
            ts2 = _("un'altra noisosissima stringa")
            tts = request.localizer.translate(ts)
            tts2 = request.localizer.translate(ts2)
            l.info ('vediamo che ne esce: {}, {}'.format(tts,tts2))

            tsv = _("questa stringa contiene {num} numero")
            for i in range(5):
                l.info("{}".format(i))
                l.info(tsv.format(num=i))
                l.info(request.localizer.translate(tsv,domain='pyramid_test').format(num=i) )
                l.info(request.localizer.pluralize(tsv,'',i,domain='pyramid_test' ).format(num=i) )
                #l.info( request.localizer.translate(tsv,mapping={'num':i+1}) )
        except DBAPIError:
            return Response(db_err_msg, content_type='text/plain', status=500)
        return {'one': one, 'project': 'pyramid_test','msg' : ts, 'msg2':ts2,'msg3':tsv}


db_err_msg = """\
Pyramid is having a problem using your SQL database.  The problem
might be caused by one of the following things:

1.  You may need to initialize your database tables with `alembic`.
    Check your README.txt for descriptions and try to run it.

2.  Your database server may not be running.  Check that the
    database server referred to by the "sqlalchemy.url" setting in
    your "development.ini" file is running.

After you fix the problem, please restart the Pyramid application to
try it again.
"""
