from pyramid.config import Configurator
from pyramid.i18n import get_localizer

def add_renderer_globals(event):
    # ...
    request = event['request']
    event['_'] = request.translate
    event['_p'] = request.pluralize
    event['localizer'] = request.localizer

#tsf = TranslationStringFactory('YOUR_GETTEXT_DOMAIN')
from .utils import _ as tsf

def add_localizer(event):
    request = event.request
    localizer = get_localizer(request)
    defaultdomain = tsf('').domain
    def auto_translate(*args, **kwargs):
        return localizer.translate(tsf(*args, **kwargs))
    def auto_pluralize(src,num, *args,**kwargs):
        thisdomain = kwargs['domain'] if 'domain' in kwargs else defaultdomain
        return localizer.pluralize(tsf(src,*args, **kwargs),'',num,domain=thisdomain)
    request.localizer = localizer
    request.translate = auto_translate
    request.pluralize = auto_pluralize

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    with Configurator(settings=settings) as config:
        config.add_translation_dirs('pyramid_test:locale/')
        config.include('.models')
        config.include('pyramid_jinja2')
        config.include('pyramid_mako')
        config.add_subscriber(add_renderer_globals,
                          'pyramid.events.BeforeRender')
        config.add_subscriber(add_localizer,
                          'pyramid.events.NewRequest')
        config.include('ppss_auth')
        config.include('.routes')
        config.scan()
    return config.make_wsgi_app()
