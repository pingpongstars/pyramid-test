<%inherit file="layout.mako"/>



<div class="content">

  <%include file="ppss_auth:templates/partials/userblock.mako" />

  <h1><span class="font-semi-bold">Pyramid</span> <span class="smaller">${_("title")}</span></h1>
  <h2>${_(msg)}</h2>
  <p>${_(msg2)}</p>
  <p>${localizer.translate(msg2)}</p>



  <p class="lead">Welcome to <span class="font-normal">${project}</span>, a&nbsp;Pyramid application generated&nbsp;by<br><span class="font-normal">Cookiecutter</span>.</p>
  <ul>
    % for k in range(5):
      <li>${_p(msg3,k).format(num=k)}</li>
    % endfor
  </ul>
</div>

