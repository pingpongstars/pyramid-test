from pyramid.i18n import TranslationStringFactory
_ = TranslationStringFactory('pyramid_test')

#__ = TranslationStringFactory('pyramid_test')

def __(request,mystr,*args,**kwargs):
  ts = _(mystr,*args,**kwargs)
  return request.localizer.translate(mystr)