"""Pyramid bootstrap environment. """
from alembic import context
from pyramid.paster import get_appsettings, setup_logging
from sqlalchemy import engine_from_config

from pyramid_test.models.meta import Base

config = context.config

setup_logging(config.config_file_name)

settings = get_appsettings(config.config_file_name)
target_metadata = Base.metadata


from ppss_auth.ppss_auth_utils.db import exclude_ppss_auth_tables

#import re
#def exclude_tables_from_config(config_):
#    tables_ = config_.get("pattern", None)
#    if tables_ is not None:
#        print ("tables_:{}".format(tables_))
#        #tables = list(map(lambda x: re.compile(x.strip()),tables_.split(",")))
#        tables = list(
#            map(lambda x: re.compile("^" + x.strip().replace("*","(.*)") + "$")
#                ,str.splitlines(tables_)
#            ) 
#        )
#        print ("tables:{}".format(tables))
#    return tables
#
##exclude_tables = exclude_tables_from_config(config.get_section('alembic:exclude'))
#
#
#def include_object(object, name, type_, reflected, compare_to):
#    if type_ == "table":
#        for i in exclude_tables:
#            if exclude_tables.match(name):
#                return False
#    return True
#    #if type_ == "table" and name in exclude_tables:
#    #    return False
#    #else:
#    #    return True


def run_migrations_offline():
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    context.configure(url=settings['sqlalchemy.url'],**exclude_ppss_auth_tables())
    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    engine = engine_from_config(settings, prefix='sqlalchemy.')

    connection = engine.connect()
    context.configure(
        connection=connection,
        target_metadata=target_metadata,**exclude_ppss_auth_tables()
    )

    try:
        with context.begin_transaction():
            context.run_migrations()
    finally:
        connection.close()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
